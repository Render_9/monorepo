# CircleCI for Monorepo

version Bitbucket

# This is 何。

[Comsbi-saas](https://bitbucket.org/sonicmoov/comsbi-saas/src/develop/)のように、１つのディレクトリで複数プロジェクトを管理している状態、管理方法を ”モノリポジトリ” と呼びます。

CircleCI の設定ファイルは root に置く必要があるため、普通に使用していてはモノリポジトリに対応できません。
このリポジトリはモノリポジトリな構成ですが、CI を走らせることができます。

```
.circleci
   ├── circle_trigger.sh
   └── config.yml
```

## ファイル構成

[Comsbi-saas](https://bitbucket.org/sonicmoov/comsbi-saas/src/develop/)と同じにしています。
中身はダミーデータです。

## text
